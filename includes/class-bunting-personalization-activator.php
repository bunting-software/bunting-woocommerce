<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/includes
 * @author     Your Name <email@example.com>
 */
class Bunting_Personalization_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		add_option( "bunting_personalization_db_version", "1.0.0" );
		add_option( "bunting_personalization_bunting_id", "", "", "yes" );
		add_option( "bunting_personalization_bunting_email" );
		add_option( "bunting_personalization_bunting_website_monitor_id" );
		add_option( "bunting_personalization_bunting_unique_code" );
		add_option( "bunting_personalization_bunting_subdomain" );
		add_option( "bunting_personalization_feed_token" );
		add_option( "bunting_personalization_password_api" );
	}

}
