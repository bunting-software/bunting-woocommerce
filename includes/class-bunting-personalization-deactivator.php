<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/includes
 * @author     Your Name <email@example.com>
 */
class Bunting_Personalization_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		delete_option( "bunting_personalization_db_version" );
		delete_option( "bunting_personalization_bunting_id" );
		delete_option( "bunting_personalization_bunting_email" );
		delete_option( "bunting_personalization_bunting_website_monitor_id" );
		delete_option( "bunting_personalization_bunting_unique_code" );
		delete_option( "bunting_personalization_bunting_subdomain" );
		delete_option( "bunting_personalization_feed_token" );
		delete_option( "bunting_personalization_password_api" );
	}

}
