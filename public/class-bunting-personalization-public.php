<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/public
 * @author     Your Name <email@example.com>
 */
class Bunting_Personalization_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	function bunting_tracking_js() {
		
		$bunting_id = get_option('bunting_personalization_bunting_id');
		
		if ($bunting_id && $bunting_id != '') {

			$output = '';
			
			if (is_front_page()) {
				ob_start(); ?>
				<!-- Bunting homepage script -->
				<script type="text/javascript">if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; $_Bunting.d.hp = 'yes';</script>
				<?php
				$output .= ob_get_clean();
			}

			if (class_exists('WooCommerce')) {
				if (is_product()) {
					global $post;

					$product_id = $post->ID;
                    $product = new WC_Product($product_id);
                    $currency = strtolower(get_woocommerce_currency());
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'post-thumbnail');
                    ob_start(); ?>

					<!-- Bunting product ID script - place ABOVE Generic Head Code -->
					<script type="text/javascript">
						if (typeof window.$_Bunting == "undefined") window.$_Bunting = {d: {}};

						$_Bunting.d.vp_upc = "<?php echo $product_id; ?>";
                        $_Bunting.d.vp_ps_<?php echo $currency; ?> = "<?php echo number_format($product->get_price(), 2); ?>";
                        $_Bunting.d.vp_iu = "<?php echo isset($image[0]) ? $image[0] : ''; ?>";

                        <?php if ($product->is_on_sale()) {
                            $saving = $product->get_regular_price() - $product->get_sale_price(); ?>
                            $_Bunting.d.vp_oss_<?php echo $currency; ?> = "<?php echo number_format($saving); ?>";
                            <?php if($product->get_date_on_sale_to()) {
                                list($to) = explode('T', $product->get_date_on_sale_to()); ?>
                                $_Bunting.d.vp_iu = "<?php echo DateTime::createFromFormat('Y-m-d', $to)->format('U'); ?>";
                            <?php } ?>
                        <?php } ?>
					</script>
					<?php
					$output .= ob_get_clean();

				} elseif (is_cart()) {

					$cart = WC()->session->get('cart');

					ob_start(); ?>

					<!-- Bunting shopping cart script - place ABOVE Generic Head Code -->
					<script type="text/javascript">
						if (typeof $_Bunting == "undefined") var $_Bunting = {d: {}}; // Do not edit
						$_Bunting.d.cp = new Array(); // Do not edit

						$_Bunting.d.cdc = "";  // Recommended. Delivery cost of the cart (number)

						<?php foreach ($cart as $product): ?>

						// Repeat the following code for each product in cart - start here
						$_Bunting.d.cp.push([
							"<?= $product['product_id'] ?>",     // Mandatory. Unique product ID used by your system (text). Eg: "7493"
							"<?= $product['line_total'] ?>",     // Mandatory. Product price in GREAT BRITISH POUNDS (number). Eg: "349.95"
							"<?= $product['quantity'] ?>"      // Mandatory. Quantity in cart (number). Eg: "1"
						]);

						<?php endforeach; ?>

						// End cart product repetition here
					</script>
					<?php
					$output .= ob_get_clean();

				} elseif (is_order_received_page()) {

					$order_id = wc_get_order_id_by_order_key($_GET['key']);

					$order = new WC_Order($order_id);

					$products = $order->get_items();

					$bunting_unique_code = get_option('bunting_personalization_bunting_unique_code');

					ob_start(); ?>

					<!-- Bunting order complete script - place ABOVE Generic Head Code -->
					<script type="text/javascript">
						if (typeof $_Bunting == "undefined") var $_Bunting = {d: {}}; // Do not edit
						$_Bunting.d.uc = "<?= $bunting_unique_code ?>"; // Do not edit
						$_Bunting.d.op = new Array(); // Do not edit

						// Edit from here onwards...

						$_Bunting.d.uoc = "<?= $order_id ?>";  // Unique order ID generated by your system (text)
						$_Bunting.d.odc = "<?= $order->calculate_shipping() ?>";  // Delivery cost of the order (number)

						<?php foreach($products as $product): ?>

						// Repeat the following code for each product that has been ordered - start here
						$_Bunting.d.op.push([
							"<?= $product['product_id'] ?>",     // Unique product ID used by your system (text). Eg: "7493"
							"<?= $product['line_total'] ?>",     // Product price in GREAT BRITISH POUNDS (number). Eg: "349.95"
							"<?= $product['qty'] ?>"      // Quantity in cart (number). Eg: "1"
						]);

						<?php endforeach; ?>

						// End ordered product repetition here
					</script>
					<?php
					$output .= ob_get_clean();
				} elseif (is_checkout()) {
					ob_start(); ?>
					<!-- Bunting checkout script -->
					<script type="text/javascript">if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; $_Bunting.d.co = 'yes';</script>
					<?php
					$output .= ob_get_clean();
				} elseif (is_product_category()) {

					global $wp_query;
					$cat_obj = $wp_query->get_queried_object();

					$category_string = get_category_parents( $cat_obj->term_id, false, '>', true );
					$category_string = substr($category_string, 0, -1) . (strlen($category_string) ? '>' : '') . $cat_obj->name;
					
					ob_start(); ?>
					<!-- Bunting category script -->
					<script type="text/javascript">
						if (typeof window.$_Bunting=="undefined") window.$_Bunting={d:{}}; // Do not edit

						$_Bunting.d.c = "<?php echo $category_string; ?>";     // Enter the category name as used by your system here
					</script>
					<?php
					$output .= ob_get_clean();
				}
			}

			$user_ID = get_current_user_id();
			$bunting_subdomain = get_option('bunting_personalization_bunting_subdomain');
			$bunting_website_monitor_id = get_option('bunting_personalization_bunting_website_monitor_id');
            $region_id = get_option('bunting_personalization_bunting_server_region_subdomain_id');

			ob_start(); ?>

			<!-- Additional visitor information, plus currency/language switch - place ABOVE Generic Head Code -->
			<script type="text/javascript">
				if (typeof window.$_Bunting == 'undefined') window.$_Bunting = {d: {}}; // Do not edit
				
				<?php if ($user_ID): ?>
				$_Bunting.d.uac = "<?php echo $user_ID ?>";

				<?php $user_info = get_user_by('id', $user_ID); ?>
				$_Bunting.d.fn = "<?php echo $user_info->first_name ?>"; // The visitor's forename, if known
				$_Bunting.d.sn = "<?php echo $user_info->last_name ?>"; // The visitor's surname, if known
				$_Bunting.d.ea = "<?php echo $user_info->user_email ?>"; // The visitor's email address, if known
				$_Bunting.d.g = "";  // The visitor's gender. Enter 'male' or 'female' if known, or leave empty

				<?php endif; ?>

			</script>

			<!-- Bunting Asynchronous tracking code-->
			<script type="text/javascript">(function () {
					if (typeof window.$_Bunting == "undefined")window.$_Bunting = {d: {}};
					$_Bunting.src = ("https:" == document.location.protocol ? "https://" : "http://") + "<?php echo $bunting_subdomain . (is_numeric($region_id) ? '.' . $region_id : ''); ?>.bunting.com/call.js?wmID=<?= $bunting_website_monitor_id; ?>";
					$_Bunting.s = document.createElement("script");
					$_Bunting.s.type = "text/javascript";
					$_Bunting.s.async = true;
					$_Bunting.s.defer = true;
					$_Bunting.s.charset = "UTF-8";
					$_Bunting.s.src = $_Bunting.src;
					document.getElementsByTagName("head")[0].appendChild($_Bunting.s)
				})()</script>

			<?php
			$output .= ob_get_clean();

			echo $output;
		}
	}

	function bunting_parse_request( &$wp ) {
		if (isset($wp->query_vars['pagename']) && $wp->query_vars['pagename'] == 'bunting-xml.php') {
            include( dirname(__FILE__) . DIRECTORY_SEPARATOR . 'bunting-xml.php');
            exit();
		}
		return;
	}

}
