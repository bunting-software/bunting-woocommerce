<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE feed SYSTEM "https://<?= $bunting_subdomain . (is_numeric($region_id) ? '.' . $region_id : ''); ?>.bunting.com/feed-<?= $bunting_website_monitor_id; ?>.dtd">
<feed last_page="<?php echo $last_page; ?>">
    <?php while ($loop->have_posts()) {
        $loop->the_post();
        $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'single-post-thumbnail');
        $terms = get_the_terms(get_the_ID(), 'product_cat');

        if (!isset($image[0]) || !isset($terms[0])) {
            continue;
        }

        $product = new WC_Product(get_the_ID()); ?>
        <product>
            <upc><?php the_ID(); ?></upc>
            <ns><<?php echo $language; ?>><![CDATA[<?php the_title(); ?>]]></<?php echo $language; ?>></ns>
            <ps><<?php echo $currency; ?>><?php echo number_format($product->get_price(), 2, '.', ''); ?></<?php echo $currency; ?>></ps>
            <u><![CDATA[<?php the_permalink(); ?>]]></u>
            <?php if (isset($image[0])) { ?>
                <iu><![CDATA[<?php echo $image[0]; ?>]]></iu>
            <?php } ?>
            <?php if (isset($terms[0])) {
                $category_string = get_category_parents($terms[0]->term_id, false, '>', true); ?>
                <c><![CDATA[<?php echo substr($category_string, 0, -1) . (strlen($category_string) ? '>' : '') . $terms[0]->name; ?>]]></c>
            <?php } ?>
            <?php if ($product->is_on_sale()) { ?>
                <?php $saving = $product->get_regular_price() - $product->get_sale_price(); ?>
                <oss><<?php echo $currency; ?>><?php echo number_format($saving, 2, '.', ''); ?></<?php echo $currency; ?>></oss>
                <?php if($product->get_date_on_sale_to()) {
                    list($to) = explode('T', $product->get_date_on_sale_to()); ?>
                    <oe><?php echo DateTime::createFromFormat('Y-m-d', $to)->format('U'); ?></oe>
                <?php } ?>
            <?php } ?>
            <s><?php echo $product->is_in_stock() ? 'y' : 'n'; ?></s>
            <?php $tags = get_the_terms(get_the_ID(), 'product_tag');
            $tags = array_map(function($tag) { return $tag->name; }, is_array($tags) ? $tags : array());
            if (count($tags)) { ?>
                <kw><![CDATA[<?php echo implode(',',$tags); ?>]]></kw>
            <?php }

            if ($product->get_sku()) { ?>
                <gtin><![CDATA[<?php $product->get_sku(); ?>]]></gtin>
            <?php } ?>
        </product>
    <?php } ?>
</feed>