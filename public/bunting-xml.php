<?php

global $wpdb;

$feed_token = isset($_GET['feed_token']) ? $_GET['feed_token'] : false;
$feed_token_option = get_option('bunting_personalization_feed_token');
$bunting_subdomain = get_option('bunting_personalization_bunting_subdomain');
$bunting_website_monitor_id = get_option('bunting_personalization_bunting_website_monitor_id');
$region_id = get_option('bunting_personalization_bunting_server_region_subdomain_id');

if (
    $feed_token && 
    $feed_token == $feed_token_option && 
    $bunting_subdomain && 
    $bunting_website_monitor_id
) {
    if (isset($_GET['size'])) {
        $limit = (int) $_GET['size'];
        if ($limit <= 0) {
            $limit = 1;
        }
    } else {
        $limit = 200;
    }
    $page = isset($_GET['page']) ? (int) $_GET['page'] + 1 : 1;
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => $limit,
        'post_status' => 'publish',
        'paged' => $page
    );
    $loop = new WP_Query( $args );

    $last_page = 'yes';

    if ($loop->post_count == $limit) {
        
        $total = $loop->found_posts;

        $last_page_number = ceil($total / $limit);

        if ($page != $last_page_number) {
            $last_page = 'no';
        }
    }
    
    $currency = strtolower(get_woocommerce_currency());
    list($language, $locale) = explode('_', get_locale());
    $bunting_products = [];
    if ($loop->have_posts()) {
        header('Content-Type: application/xml');
        include_once('partials/bunting-xml-display.php' );
        exit;
    }
}

status_header( 400 );
nocache_headers();
exit;
