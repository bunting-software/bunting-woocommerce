<?php include_once('header.php'); ?>
<div class="box-container">
    <h2 class="title">Bunting scripts <strong>removed successfully</strong></h2>
    <p>The Bunting tracking script has been successfully removed from your store. Bunting is no longer tracking your visitors.</p>
    <br><br>
    <h3>Uninstalling the app</h3>
    <p>Go to "Modules and Services" in the admin menu to the left, search for Bunting Personalization, click uninstall in the dropdown menu.</p>
    <br>
    <p><strong>We're sorry to see you go, but thank you for using Bunting!</strong></p>
    <br>
    <a href="<?php echo admin_url('?page=bunting-personalization'); ?>" class="btn btn-primary btn-lg" >Back to the install page</a>
</div>
<?php include_once('footer.php'); ?>