<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/admin/partials
 */
?>

<?php include_once('header.php'); ?>

<?php if ( !class_exists( 'WooCommerce' ) ): ?>
    <div class="notice notice-warning is-dismissible bunting-warning">
        <p>Woocommerce is required to enable all Bunting features.</p>
    </div>
<?php endif; ?>

<div class="box-container logged-in">
    <?php if ($message != ''): ?>
    <h2 class="title">Bunting added <strong>Successfully</strong></h2>
    <p><?= $message; ?></p>
    <?php else: ?>
    <h2 class="title">Sign in to <strong>Bunting</strong></h2>
    <?php endif; ?>
    <p>
        Account address:<br><strong><?= $bunting_subdomain; ?>.bunting.com</strong>
    </p>
    <form action="https://<?= $bunting_subdomain; ?>.bunting.com/login" class="dotted go-to-bunting" target="_blank" method="post">
        <input type="hidden" name="a" value="login">
        <input type="hidden" name="timestamp" value="<?= timestamp; ?>">
        <input type="hidden" name="hash" value="<?= hash; ?>">
        <input type="hidden" name="password_api" value="<?= password_api; ?>">
        <input type="hidden" name="email_address" value="<?= email_address; ?>">
        <input type="hidden" name="plugin" value="wordpress">
        <?php if ($message != ''): ?>
        <input type="hidden" name="redirect" value="/account?a=show_registration_complete">
        <?php endif; ?>
        <button type="submit" class="btn btn-info">
            Login to your<br>Bunting Account
        </button>
    </form>
    <a href="<?php echo admin_url('?page=bunting-personalization&unlink=true'); ?>" onclick="return confirm('Are you sure you want to unlink your Bunting account?');" class="unlink-bunting btn btn-default">
        Uninstall
    </a>
</div>
<script>
    setInterval(function() {
        window.location.reload();
    }, 58000);
</script>

<?php include_once('footer.php'); ?>