<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Bunting_Personalization
 * @subpackage Bunting_Personalization/admin
 */
class Bunting_Personalization_Admin {
	/** @var string $plugin_name */
	private $plugin_name;

	/** @var string $version */
	private $version;

	/**
	 * @param string $plugin_name
	 * @param string $version
     * @return void
	 */
	public function __construct($plugin_name, $version) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 * @return void
	 */
	public function enqueue_styles() {
        wp_enqueue_style( $this->plugin_name . '-bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css');
		wp_enqueue_style( $this->plugin_name . '-admin', plugin_dir_url( __FILE__ ) . 'css/admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name . '-font', '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin');

	}

	/**
	 * Register the JavaScript for the admin area.
	 * @return void
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( 'validate', plugin_dir_url( __FILE__ ) . 'js/validate.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/admin.js', array( 'jquery','validate' ), $this->version, false );
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 * @return void
	 */
	public function add_plugin_admin_menu() {
		if (current_user_can('manage_options')) {
			add_menu_page(
				'Bunting Personalization',
				'Personalization',
				'manage_options',
				$this->plugin_name,
				array($this, 'display_plugin_setup_page'),
				'dashicons-feedback',
				50
			);
		}
	}

	 /**
	 * Add settings action link to the plugins page.
     * @param array $links
	 * @return array
	 */
	public function add_action_links($links) {
	   $settings_link = array(
	       '<a href="' . admin_url('admin.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
	   );

	   return array_merge($settings_link, $links);
	}

	/**
	 * Render the settings page for this plugin.
     * @return void
	 */
	public function display_plugin_setup_page() {
		if (!current_user_can('manage_options' )) {
			wp_die(__( 'You do not have sufficient permissions to access this page.'));
		}
		
		if (isset($_REQUEST['verify_email_address'])) {
			$this->buntingVerify();
		} elseif (isset($_REQUEST['register_email_address'])) {
			$this->buntingRegister();
		}

        if (isset($_REQUEST['subdomain'])) {
		    $this->existsAction();
        }

		if (isset($_REQUEST['unlink'])) {
			$this->unlinkBunting();
			include_once( 'partials/bunting-personalization-admin-unlinked.php' );
			exit();
		}

		$bunting_id = get_option('bunting_personalization_bunting_id');
		if ($bunting_id) {
			$this->displayHome();
		}
		else {
			$this->displayInstall();
		}
	}

    /**
     * @return void
     */
	public function displayInstall() {
		$admin_email = get_option('admin_email');
		$blogname = get_option('blogname');
		$potential_subdomain = urlencode($blogname);
		include_once('partials/bunting-personalization-admin-install.php');
	}

    /**
     * @return void
     */
	public function displayHome() {
		$timestamp = time();
		$bunting_subdomain = get_option("bunting_personalization_bunting_subdomain");
		$email_address = get_option("bunting_personalization_bunting_email");
		$password_api = get_option("bunting_personalization_bunting_password_api");
		$account_key = $bunting_subdomain.$email_address.$timestamp;
		$hash = hash_hmac('sha256', $account_key, '6s@dKsD-3a&-1kP');
		$message = '';

		if (get_transient('bunting_message')) {
			$message = get_transient('bunting_message');
			delete_transient('bunting_message');
		}

		include_once('partials/bunting-personalization-admin-home.php');
	}

    /**
     * Checks whether a remove subdomain exists within Bunting
     * This is a bit of a hack and once Bunting supports a better mechanism, we should use that instead
     * @return void
     */
    public function existsAction() {
        $subdomain = $_REQUEST['subdomain'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://' . $subdomain . '.1.bunting.com/login?a=lost_password');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        $response = curl_exec($ch);

        $this->clearBuffer();
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
            echo 1;
            exit();
        }

        echo 0;
        exit();
    }

    /**
     * @return void
     */
	private function buntingVerify() {
		$bunting_data = $this->submitToBunting('verify', [
			'email_address' => $_REQUEST['verify_email_address'],
			'password' => $_REQUEST['verify_password'],
			'subdomain' => $_REQUEST['verify_bunting_subdomain']
		]);
		$this->buntingResponse($bunting_data);
	}

    /**
     * @return void
     */
	private function buntingRegister() {
		list($language, $locale) = explode('_', get_locale());

		$submit_data = [
			'billing' => 'automatic',
			'email_address' => $_REQUEST['register_email_address'],
			'password' => $_REQUEST['register_password'],
			'confirm_password' => $_REQUEST['password_confirmation'],
			'subdomain' => $_REQUEST['register_bunting_subdomain'],
			'name' => $_REQUEST['company_name'],
			'forename' => $_REQUEST['forename'],
			'surname' => $_REQUEST['surname'],
			'telephone_number' => $_REQUEST['telephone_number'],
			'promotional_code' => $_REQUEST['promotional_code'],
			'timezone' => get_option('timezone_string'),
			'country' => $locale,
			'agency' => 'no'
		];

		$bunting_data = $this->submitToBunting('register', $submit_data);
		$this->buntingResponse($bunting_data);
	}

    /**
     * @param $action
     * @param $params
     * @return array|mixed|object
     */
	private function submitToBunting($action, $params) {
		list($language, $locale) = explode('_', get_locale());
		$languages = [
			strtoupper($language)
		];
		$currencies = [
			[
				'currency' => get_woocommerce_currency(),
				'symbol' => get_woocommerce_currency_symbol()
			]
		];

		$siteurl = get_option('siteurl');
		$domain = parse_url($siteurl, PHP_URL_HOST);
		$timestamp = time();
		$feed_token = md5($timestamp);

		$default_params = [
			'timestamp' => $timestamp,
			'hash' => hash_hmac('sha256', $timestamp, '6s@dKsD-3a&-1kP'),
			'plugin' => 'woocommerce',
			'domain_name' => $domain,
			'create_website_monitor' => 'yes',
			'website_name' => get_option('blogname'),
			'languages' => $languages,
			'currencies' => $currencies,
			'website_platform' => 'Wordpress',
			'ecommerce' => 'yes',
			'cart_url' => WC_Cart::get_cart_url(),
			'product_feed-url_protocol' => parse_url($siteurl, PHP_URL_SCHEME),
			'product_feed-url' => $domain.'/?pagename=bunting-xml.php&feed_token='.$feed_token
		];
		$params = $params+$default_params;
		$params = $this->httpBuildQueryForCurl($params);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.bunting.com/plugins/'.$action);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);

		if ($response === false) {
			$responseData = ['errors' => ['network' => 'Unable to connect to Bunting api']];
		} else {
			$responseData = json_decode($response, true);
		}

		if ($responseData['success']) {
			$responseData['email_address'] = $params['email_address'];
			$this->installBunting($responseData, $feed_token);
		}
		return $responseData;
	}

    /**
     * @param $bunting_data
     */
	private function buntingResponse($bunting_data)
	{
		if ($bunting_data['success']) {
			$response_array = ['message' => 'Success'];
			set_transient('bunting_message', 'You can now login to Bunting.', 12 * HOUR_IN_SECONDS);
		} else {
			$response_array = ['message' => 'Please review the errors and try again.', 'errors' => $bunting_data['errors']];
		}

        $this->clearBuffer();
		header('Content-Type: application/json');
		wp_send_json($response_array);
		exit();
	}

    /**
     * Fixes a bug where already rendered sections of pages need to be removed from the output buffer
     * @return void
     */
	private function clearBuffer() {
        if (ob_get_level()) {
            ob_clean();
        }
    }

    /**
     * @param $arrays
     * @param array $new
     * @param null $prefix
     * @return array
     */
	private function httpBuildQueryForCurl($arrays, &$new = array(), $prefix = null) {
		if (is_object($arrays)) {
			$arrays = get_object_vars($arrays);
		}

		foreach ($arrays as $key => $value) {
			$k = isset($prefix) ? $prefix.'['.$key.']' : $key;
			if (is_array($value) || is_object($value)) {
				$this->httpBuildQueryForCurl($value, $new, $k);
			} else {
				$new[$k] = $value;
			}
		}

		return $new;
	}

    /**
     * @param $data
     * @param $feed_token
     */
	private function installBunting($data, $feed_token) {
		update_option("bunting_personalization_bunting_id", $data['account_id']);
		update_option("bunting_personalization_bunting_email", $data['email_address']);
		update_option("bunting_personalization_bunting_website_monitor_id", $data['website_monitor_id']);
		update_option("bunting_personalization_bunting_unique_code", $data['unique_code']);
		update_option("bunting_personalization_bunting_subdomain", $data['subdomain']);
        update_option("bunting_personalization_bunting_server_region_subdomain_id", $data['server_region_subdomain_id']);
		update_option("bunting_personalization_feed_token", $feed_token );
		update_option("bunting_personalization_password_api", $data['password_api']);
	}

    /**
     * Remove all saved bunting data
     *
     * @return bool
     */
	private function unlinkBunting() {
		update_option("bunting_personalization_bunting_id", "");
		update_option("bunting_personalization_bunting_email", "");
		update_option("bunting_personalization_bunting_website_monitor_id", "");
		update_option("bunting_personalization_bunting_unique_code", "");
		update_option("bunting_personalization_bunting_subdomain", "");
        update_option("bunting_personalization_bunting_server_region_subdomain_id", "");
		update_option("bunting_personalization_feed_token", "");
		update_option("bunting_personalization_password_api", "");
		return true;
	}
}
