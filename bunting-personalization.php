<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://bunting.com/
 * @since             1.0.0
 * @package           Bunting_Personalization
 *
 * @wordpress-plugin
 * Plugin Name:       Bunting Personalization
 * Plugin URI:        http://bunting.com/
 * Description:       Website personalization tool.
 * Version:           1.0.0
 * Author:            Bunting
 * Text Domain:       bunting-personalization
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bunting-personalization-activator.php
 */
function activate_bunting_personalization() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bunting-personalization-activator.php';
	Bunting_Personalization_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bunting-personalization-deactivator.php
 */
function deactivate_bunting_personalization() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bunting-personalization-deactivator.php';
	Bunting_Personalization_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_bunting_personalization' );
register_deactivation_hook( __FILE__, 'deactivate_bunting_personalization' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bunting-personalization.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bunting_personalization() {

	$plugin = new Bunting_Personalization();
	$plugin->run();

}
run_bunting_personalization();
