# Installing Bunting on WooCommerce

To download the plugin click on Downloads in the left hand menu then click the `Download Repository` link.

To install the `bunting-woocommerce` plugin, go the admin dashboard of your site and within the menu on the left click `Plugins` 
then `Add New`, this will navigate you to a plugins listing page on the top of this page should be an `Upload Plugins` button
 which is shown to the right of the "Add Plugins" header. 

Next, press `choose file` and navigate where you downloaded our `bunting-woocommerce.zip` then click open. 

>Please note that this file must be a `.zip` file. If you downloaded the bunting-woocommerce folder just zip up the folder and then retry the upload.

Once this has been done press `Install Now`, if it has worked successfully then you should get a message saying "Plugin installed successfully." 
 and underneath is an `Activate Plugin` button. Please press this. 

You will then be redirected to your plugins list which should have `Bunting Personalization` displayed within it. 

This will have created a `Personalization` tab on your Wordpress menu bar (the same one that you used to navigate to your Plugins page.)
Navigate here and sign in to your Bunting account to complete the installation process:
Enter your existing Bunting account details or if you are new to Bunting and want to get started, click the `CREATE ACCOUNT` button.